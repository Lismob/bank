package org.example.application;

import org.example.bank.Bank;
import org.example.bank.FrontalSystem;

import java.util.concurrent.locks.Lock;

public class ApplicationProcessor implements Runnable {

    private Bank bank;
    private FrontalSystem frontalSystem;
    Lock lock;

    public ApplicationProcessor(Bank bank, FrontalSystem frontalSystem, Lock lock) {
        this.bank = bank;
        this.frontalSystem = frontalSystem;
        this.lock = lock;
    }

    public void run() {
        while (true) {
            lock.lock();
            if (!frontalSystem.getQueue().isEmpty()) {
                applicationProcessing();
            }
        }
    }

    public void applicationProcessing() {
        switch (frontalSystem.getQueue().element().getOperation()) {
            case CREDIT -> {
                bank.giveALoan(frontalSystem.getQueue().element());
                frontalSystem.getQueue().remove();
            }
            case REDEMPTION -> {
                bank.acceptMoney(frontalSystem.getQueue().element());
                frontalSystem.getQueue().remove();
            }
        }
        lock.unlock();
    }
}
