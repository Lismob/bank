package org.example.application;

import org.example.client.Client;

public class Application {

    private Client client;
    private int amountOfMoney;
    private Operation operation;

    public Application(Client client, int amountOfMoney, Operation operation) {
        this.client = client;
        this.amountOfMoney = amountOfMoney;
        this.operation = operation;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public int getAmountOfMoney() {
        return amountOfMoney;
    }

    public void setAmountOfMoney(int amountOfMoney) {
        this.amountOfMoney = amountOfMoney;
    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }
}
