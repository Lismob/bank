package org.example.client;

import org.example.application.Application;
import org.example.application.Operation;
import org.example.bank.FrontalSystem;

public class Client implements Runnable {
    private String name;
    private int balance;
    private FrontalSystem frontalSystem;

    public Client(String name, int balance, FrontalSystem frontalSystem) {
        this.name = name;
        this.balance = balance;
        this.frontalSystem = frontalSystem;
    }

    public Application loanRepayment() {
        return new Application(this, 5, Operation.REDEMPTION);
    }

    public Application takeLoan() {
        return new Application(this, 10, Operation.CREDIT);
    }
   public void writingOffMoney(int value){
        balance-=value;
    }
    public void replenishmentOfFunds(int value){
        balance+=value;
    }

    public int getBalance() {
        return balance;
    }

    public String getName() {
        return name;
    }

    public synchronized void  run(){
        try {
                if (balance >= 5) {
                    System.out.println(name + " Отправил заявку на вложение средств !");
                    frontalSystem.getQueue().put(loanRepayment());
                } else {
                    System.out.println(name + " Отправил заявку на кредитование !");
                    frontalSystem.getQueue().put(takeLoan());
                }
                Thread.sleep(1000);
        }
            catch (InterruptedException in){
                in.printStackTrace();
            }
      }
}
