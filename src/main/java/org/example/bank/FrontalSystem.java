package org.example.bank;

import org.example.application.Application;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class FrontalSystem {
    private BlockingQueue<Application> queue=new ArrayBlockingQueue(4);

    public BlockingQueue<Application> getQueue() {
        return queue;
    }
}
