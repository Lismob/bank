package org.example.bank;

import org.example.application.Application;

import java.util.concurrent.atomic.AtomicInteger;

public class Bank {
    private AtomicInteger balance;
    private FrontalSystem frontalSystemBank;


    public Bank(int balance, FrontalSystem frontalSystemBank) {
        this.balance = new AtomicInteger(balance);
        this.frontalSystemBank = frontalSystemBank;
    }

    public void giveALoan(Application application) {
        application.getClient().replenishmentOfFunds(application.getAmountOfMoney());
        balance.updateAndGet(x -> x - application.getAmountOfMoney());
        System.out.println("Банк одобрил заявку на кредит "+ application.getClient()
                .getName()
                +" Денег в банке:"+balance+" Денег у клиента:"
                +application.getClient().getBalance());
    }

    public void acceptMoney(Application application) {
        application.getClient().writingOffMoney(application.getAmountOfMoney());
        balance.updateAndGet(x -> x + application.getAmountOfMoney());
        System.out.println("Банк принял вклад от "
                + application.getClient().getName() +" Денег в банке:"
                +balance+" Денег у клиента:"+application.getClient().getBalance());
    }

    public AtomicInteger getBalance() {
        return balance;
    }

    public void setBalance(AtomicInteger balance) {
        this.balance = balance;
    }

    public FrontalSystem getFrontalSystemBank() {
        return frontalSystemBank;
    }

    public void setFrontalSystemBank(FrontalSystem frontalSystemBank) {
        this.frontalSystemBank = frontalSystemBank;
    }

}
