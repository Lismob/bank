package org.example;

import org.example.application.ApplicationProcessor;
import org.example.bank.Bank;
import org.example.bank.FrontalSystem;
import org.example.client.Client;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        Lock lock=new ReentrantLock();
        FrontalSystem frontalSystem = new FrontalSystem();
        Bank bank=new Bank(100,frontalSystem);
        ApplicationProcessor applicationProcessor1 =new ApplicationProcessor(bank,frontalSystem,lock);
       Thread applicationThred1 =new Thread(applicationProcessor1);
        Thread applicationThred2 =new Thread(applicationProcessor1);
        applicationThred1.setDaemon(true);
       applicationThred2.setDaemon(true);
       applicationThred1.start();
       applicationThred2.start();
        Client client1 = new Client("Кирилл", 10, frontalSystem);
        Client client2 = new Client("Коля", 3, frontalSystem);
        Client client3 = new Client("Саша", 0, frontalSystem);
        Client client4 = new Client("Андрей", 5, frontalSystem);

        ExecutorService executorService = Executors.newFixedThreadPool(2);
        executorService.execute(new Thread(client1));
        executorService.execute(new Thread(client2));
        executorService.execute(new Thread(client3));
        executorService.execute(new Thread(client4));
        executorService.shutdown();
    }
}
